﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Storage;

public class AccessFirebaseStorage : MonoBehaviour
{
    private void Start()
    {
        FirebaseStorage storage = FirebaseStorage.DefaultInstance;
        StorageReference storage_ref = storage.GetReferenceFromUrl("gs://map1-23a76.appspot.com");

        if (storage_ref != null)
        {
            Debug.Log("OK");
        }
        else
        {
            Debug.Log("NO");
        }
    }

}
