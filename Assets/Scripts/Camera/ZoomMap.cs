﻿using UnityEngine;

public class ZoomMap : MonoBehaviour
{
    [SerializeField] private float m_zoomSpeed = 0.5f;
    [SerializeField] private float m_limitApproximation = 2f;
    [SerializeField] private float m_limitEstrangement = 10f;
    [SerializeField] private bool m_invertZoom = true;

    private void Update()
    {
        if (Input.touchCount == 2)
        {
            Touch touch0 = Input.GetTouch(0);
            Touch touch1 = Input.GetTouch(1);

            Vector2 touch0PrevPos = touch0.position - touch0.deltaPosition;
            Vector2 touch1PrevPos = touch1.position - touch1.deltaPosition;

            var prevTouchDeltaMag = (touch0PrevPos - touch1PrevPos).magnitude;
            var touchDeltaMag = (touch0.position - touch1.position).magnitude;

            var increment = prevTouchDeltaMag - touchDeltaMag;

            increment = m_invertZoom ? increment * -1 : increment;

            Camera.main.orthographicSize += increment * m_zoomSpeed;
            Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, m_limitApproximation, m_limitEstrangement);
        }
    }
}
