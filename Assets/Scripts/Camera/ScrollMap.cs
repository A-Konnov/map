﻿using UnityEngine;

public class ScrollMap : MonoBehaviour
{
    [SerializeField] private float m_lerpSpeed = 5f;
    [SerializeField] private float m_moveSpeed = 0.1f;
    [SerializeField] private LayerMask m_mask;
    private Vector3 m_currVelocity;
    private Vector2 m_firstTapPositon;
    private Vector2 m_targetVelocity;
    private bool m_revers = false;

    private void Update()
    {
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);

            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(touch.position), Vector2.zero);
            if (Mathf.Pow(2, hit.collider.gameObject.layer) != m_mask.value)
            {
                return;
            }

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    m_firstTapPositon = Camera.main.ScreenToWorldPoint(touch.position);
                    break;

                case TouchPhase.Moved:
                    Vector2 tapPosition = Camera.main.ScreenToWorldPoint(touch.position);
                    m_targetVelocity = (tapPosition - m_firstTapPositon) * m_moveSpeed;
                    m_targetVelocity = new Vector2(m_targetVelocity.x * -1, m_targetVelocity.y * -1);
                    break;

                case TouchPhase.Ended:
                    if (IsBorderOut(m_targetVelocity))
                    {
                        m_targetVelocity = new Vector2(m_targetVelocity.x * -1, m_targetVelocity.y * -1);
                        m_revers = true;
                    }
                    else
                    {
                        m_targetVelocity = new Vector2(0, 0);
                    }
                    break;
            }
        }

        Scroll();
    }

    private void Scroll()
    {
        if (m_revers)
        {
            var velocity = new Vector2(m_targetVelocity.x * -1, m_targetVelocity.y * -1);

            if (!IsBorderOut(velocity))
            {
                m_targetVelocity = new Vector3(0, 0, 0);
                m_revers = false;
            }
        }

        m_currVelocity = Vector3.Lerp(m_currVelocity, m_targetVelocity, Time.deltaTime * m_lerpSpeed);
        transform.position = transform.position + m_currVelocity;
    }

    private bool IsBorderOut(Vector2 velocity)
    {
        var topRightCamera = Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth, Camera.main.pixelHeight));
        var bottomLeftCamera = Camera.main.ScreenToWorldPoint(new Vector2(0, 0));

        var x = velocity.x > 0 ? topRightCamera.x : bottomLeftCamera.x;
        var y = velocity.y > 0 ? topRightCamera.y : bottomLeftCamera.y;
        var targetHit = new Vector2(x, y);

        RaycastHit2D hit = Physics2D.Raycast(targetHit, Vector2.zero, float.MaxValue, m_mask);

        if (hit.collider == null) { return true; }

        return false;
    }
}

