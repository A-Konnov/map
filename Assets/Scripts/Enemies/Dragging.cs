﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class Dragging : MonoBehaviour
{
    private bool m_mouseDown = false;

    private void Update()
    {
        if (m_mouseDown)
        {
            var cursorPosition = Input.mousePosition;
            cursorPosition = Camera.main.ScreenToWorldPoint(cursorPosition);
            cursorPosition.z = -2;
            transform.position = cursorPosition;
        }
    }

    private void OnMouseDown()
    {
        m_mouseDown = true;
    }

    private void OnMouseUp()
    {
        m_mouseDown = false;
        var position = transform.position;
        position.z = -1;
        transform.position = position;        
    }

}
