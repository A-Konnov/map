﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vibrate : MonoBehaviour
{
    [SerializeField] private float m_timeVibro = 2f;

    private void OnMouseDown()
    {
        StartCoroutine(VibrateObject());
    }

    private IEnumerator VibrateObject()
    {
        var time = new WaitForSeconds(Time.deltaTime);
        var updateTime = m_timeVibro;
        while (updateTime > 0)
        {
            updateTime -= Time.deltaTime;
            Handheld.Vibrate();
            yield return time;
        }
        yield return null;
    }
}
